//@@viewOn:imports
import UU5 from "uu5g04";
import "uu5g04-bricks";
import "uu5g04-forms";

import Config from "./config/config.js";

import {JokesConsumer} from "../core/jokes-provider.js";

import "./create-form.less";
import LSI from "./create-form-lsi.js";
//@@viewOff:imports

export const Form = UU5.Common.VisualComponent.create({
  //@@viewOn:mixins
  mixins: [UU5.Common.BaseMixin, UU5.Common.PureRenderMixin],
  //@@viewOff:mixins

  //@@viewOn:statics
  statics: {
    tagName: Config.TAG + "CreateForm",
    classNames: {
      main: Config.CSS + "CreateForm"
    },
    lsi: LSI,
    opt: {
      pureRender: true
    }
  },
  //@@viewOff:statics

  //@@viewOn:propTypes
  propTypes: {
    showPublished: UU5.PropTypes.bool
  },
  //@@viewOff:propTypes

  //@@viewOn:getDefaultProps
  //@@viewOff:getDefaultProps

  //@@viewOn:reactLifeCycle
  //@@viewOff:reactLifeCycle

  //@@viewOn:interface
  //@@viewOff:interface

  //@@viewOn:overriding
  //@@viewOff:overriding

  //@@viewOn:private
  _getCategoriesOptions(categories) {
    return categories.map(category => (
      <UU5.Forms.Select.Option value={category.id} key={category.id}>
        {category.name}
      </UU5.Forms.Select.Option>
    ));
  },

  _validateText(opt) {
    let result = { feedback: Config.FEEDBACK.INITIAL, value: opt.value };
    // when there is no event, validation comes from "isValid" method
    if (opt.event === undefined) {
      if (!opt.value && !this._file.getValue()) {
        // text is empty, check file
        result.feedback = Config.FEEDBACK.ERROR;
        result.message = this.getLsiComponent("textOrFile");
        opt.component.setFeedback(result.feedback, result.message);
      }
    }
    return result;
  },

  _registerFile(cmp) {
    this._file = cmp;
  },
  //@@viewOff:private

  //@@viewOn:render
  render() {
    return (
      <JokesConsumer>
        {({ categoryList }) => (
          <UU5.Bricks.Div {...this.getMainPropsToPass()}>
            {/* // Name */}
            <UU5.Forms.TextArea
            label={this.getLsiComponent("name")}
            inputAttrs={{ maxLength: 4000 }}
            name="name"
            onValidate={this._validateText}
            autoResize
          />
            {/* // Text */}
            <UU5.Forms.TextArea
              label={this.getLsiComponent("link")}
              inputAttrs={{ maxLength: 4000 }}
              name="link"
              onValidate={this._validateText}
              autoResize
            />
            {/* // Topics */}
            <UU5.Forms.Select
              label={this.getLsiComponent("category")}
              name="categorySelect"
              multiple
              openToContent={true}
              >
              <Comment >TODO vytiahnut z databazy a strcit do foreachu </Comment>
              <UU5.Forms.Select.Option value="Sport"/>
              <UU5.Forms.Select.Option value="Auto"/>
              <UU5.Forms.Select.Option value="Doma"/>

            </UU5.Forms.Select>
          
            {/* // Author */}
            <UU5.Forms.Select
              label={this.getLsiComponent("author")}
              name="authorSelectOne"
              multiple
              openToContent={true}
              >
               {/* // @todo vytiahnut z databazy a prehnat foreachom */}
              <UU5.Forms.Select.Option value="Pewdiepie"/>
              <UU5.Forms.Select.Option value="Billions and billions"/>
              <UU5.Forms.Select.Option value="Snehurka"/>

            </UU5.Forms.Select>

            {/* // Published */}
            <UU5.Forms.DateRangePicker
              label={this.getLsiComponent("published")}
              name="published"
              placeholder="From - To"
              size="m"
            />
          </UU5.Bricks.Div>
        )}
      </JokesConsumer>
    );
  }
  //@@viewOff:render
});

export default Form;
