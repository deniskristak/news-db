export default {
  name: {
    cs: "Název",
    en: "Name"
  },
  link: {
    cs: "Link",
    en: "Link"
  },
  category: {
    cs: "Topic",
    en: "Topic"
  },
  author: {
    cs: "Autor",
    en: "Author"
  },
  published: {
    cs: "Publikován",
    en: "Published"
  },
  categorys: {
    cs: "Topics",
    en: "Topics"
  },
  textOrFile: {
    cs: "Text musí být vyplněn.",
    en: "Text must be provided."
  }
};
