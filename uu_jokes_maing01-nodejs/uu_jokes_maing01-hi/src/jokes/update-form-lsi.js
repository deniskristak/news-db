export default {
  name: {
    cs: "Název",
    en: "Name"
  },
  text: {
    cs: "Link",
    en: "Link"
  },
  image: {
    cs: "Autor",
    en: "Author"
  },
  category: {
    cs: "Kategorie",
    en: "Category"
  },
  published: {
    cs: "Publikován",
    en: "Published"
  },
  categorys: {
    cs: "Topics",
    en: "Topics"
  },
  textOrFile: {
    cs: "Text musí být vyplněn.",
    en: "Text must be provided."
  }
};
