import Lsi from "../config/lsi.js";

export default {
  list: {
    cs: "Seznam článků",
    en: "List of articles"
  },
  create: {
    cs: "Vytvořit článek",
    en: "Create article"
  },
  createHeader: {
    cs: "Vytvořit článek",
    en: "Create article"
  },
  updateHeader: {
    cs: "Upravit článek",
    en: "Update article"
  },
  deleteHeader: {
    cs: "Smazat článek",
    en: "Delete article"
  },

  deleteConfirm: {
    cs: 'Tato akce je nevratná. Opravdu chcete smazat článek s názvem "%s"?',
    en: 'This action is permanent. Are you sure you want to delete article "%s"?'
  },
  ...Lsi.buttons,

  filterByCategory: {
    cs: "Kategorie",
    en: "Category"
  },

  filterByImage: {
    cs: "Obrázku",
    en: "Image"
  },

  filterByUser: {
    cs: "Uživatele",
    en: "User"
  },

  filterByVisibility: {
    cs: "Publikace",
    en: "Published"
  },

  filterByRating: {
    cs: "Hodnocení",
    en: "Rating"
  }
};
