export default {
  createSuccessHeader: {
    cs: "Kategorie byla vytvořena",
    en: "Category was created"
  },

  createFailHeader: {
    cs: "Kategorii se nepodařilo vytvořit",
    en: "Failed to create category"
  },

  updateSuccessHeader: {
    cs: "Kategorie byla upravena",
    en: "Category was updated"
  },

  updateFailHeader: {
    cs: "Kategorii se nepodařilo upravit",
    en: "Failed to update category"
  },

  deleteSuccessHeader: {
    cs: "Kategorie byla odstraněna",
    en: "Category was deleted"
  },

  deleteFailHeader: {
    cs: "Kategorii se nepodařilo odstranit",
    en: "Failed to delete category"
  },

  rightsError: {
    cs: "K provedení akce nemáte dostatečná práva.",
    en: "You do not have sufficient rights for this action."
  },

  categoryInUseError: {
    cs:
      'Kategorie obsahuje vtipy. Aby byla kategorie smazána, musíte v dialogu smazání zaškrtnout volbu "Smazat kategorii i pokud obsahuje vtipy"',
    en:
      'Category contains jokes. To delete this category you have to check "Delete category assigned to jokes" in the confirm dialog.'
  },
  categoryNameNotUnique: {
    cs: "Název kategorie musí být unikátní.",
    en: "Category name must be unique."
  },

  unexpectedServerError: {
    cs: "Na serveru došlo k neočekávané chybě.",
    en: "Unexpected error occured"
  }
};
