import Lsi from "../config/lsi.js";

export default {
  list: {
    cs: "Seznam kategorií",
    en: "List of categories"
  },
  create: {
    cs: "Vytvořit kategorii",
    en: "Create category"
  },
  createHeader: {
    cs: "Vytvořit kategorii",
    en: "Create category"
  },
  updateHeader: {
    cs: "Upravit kategorii",
    en: "Update category"
  },
  deleteHeader: {
    cs: "Smazat kategorii",
    en: "Delete category"
  },
  ...Lsi.buttons
};
