export default {
  jokes: {
    cs: "Vtipy",
    en: "Jokes"
  },
  categories: {
    cs: "Kategorie",
    en: "Categories"
  },
  about: {
    cs: "O aplikaci",
    en: "About Application"
  },
  login: {
    cs: "Přihlášení",
    en: "Login"
  }
};
